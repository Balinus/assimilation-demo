# Deps
using DifferentialEquations
using DynamicalSystems
using PyPlot

# Parameters
t = collect(range(0.0, 20, step = 0.01))
σ = 10.0
β = 8/3
ρ = 28.0

# Initial conditions
X₀ = zeros(3) .+ 5.0

# lorenz System
@inline @inbounds function loop(u, p, t)
    σ = p[1]; ρ = p[2]; β = p[3]
    du1 = σ*(u[2]-u[1])
    du2 = u[1]*(ρ-u[3]) - u[2]
    du3 = u[1]*u[2] - β*u[3]
    return SVector{3}(du1, du2, du3)
end
# Jacobian:
@inline @inbounds function loop_jac(u, p, t)
    σ, ρ, β = p
    J = @SMatrix [-σ  σ  0;
    ρ - u[3]  (-1)  (-u[1]);
    u[2]   u[1]  -β]
    return J
end

ds = ContinuousDynamicalSystem(loop, X₀, [σ, ρ, β], loop_jac)

Traj_true = trajectory(ds, t[end], X₀, dt=0.01)

plot3D(Traj_true[:,1], Traj_true[:,2], Traj_true[:,3])

niter = 8 # number of times we perturb initial conditions
Traj_init = Array{Dataset}(undef, niter)

σ₂ = 1.0 # Strength of noise (variance)
for j = 1:niter
    Xic = X₀ .+ (σ₂ .* randn(3))
    Traj_init[j] = trajectory(ds, t[end], Xic, dt=0.01)
    subplot(4, 2, j)
    plot(t, Traj_true[:, 1], t, Traj_init[j][:,1])
end

# NEW MODEL
# assume we have measurements at time steps dt=0.5. We want to assimilate the date at these timesteps

# Noisy obs
tdata = range(0.0, 20.0, step=0.5)
n = length(tdata)
xn = randn(n); yn = randn(n); zn = randn(n);
σ₃ = 4.0 # Measurement error (e.g. variance of the sensor!)
xdata = Traj_true[1:50:end,1] .+ σ₃ .* xn
ydata = Traj_true[1:50:end,2] .+ σ₃ .* yn
zdata = Traj_true[1:50:end,3] .+ σ₃ .* zn

figure()
plot(t, Traj_true[:,1], tdata, xdata, "o")



function assimilate(ds, Xic, xdata, ydata, zdata, K)

    # Now it's time to assimilate
    x_da = Array{Float64}(undef, 0) # placeholder for assimilated data
    y_da = Array{Float64}(undef, 0) # placeholder for assimilated data
    z_da = Array{Float64}(undef, 0) # placeholder for assimilated data

    # Xic = [xdata[1], ydata[1], zdata[1]]

    for j = 1:length(tdata)-1
        tspan = range(0.0, 0.5, step=0.01) # we'll run the model for this timespan, until we get a new measurement 0.5s later
        global Traj_new = trajectory(ds, tspan[end], Xic, dt=0.01) # new model run

        Xic0 = [Traj_new[end,1], Traj_new[end,2], Traj_new[end,3]] # last conditions
        xdat = [xdata[j+1], ydata[j+1], zdata[j+1]] # measurement at final time step

        Xic = Xic0 + (K*(xdat-Xic0)) # adding Kalman innovation to modeled value

        x_da = [x_da; Traj_new[1:end-1, 1]]
        y_da = [y_da; Traj_new[1:end-1, 2]]
        z_da = [z_da; Traj_new[1:end-1, 3]]

    end

    x_da = [x_da; Traj_new[end, 1]]
    y_da = [y_da; Traj_new[end, 2]]
    z_da = [z_da; Traj_new[end, 3]]

    return x_da, y_da, z_da



end

K = σ₂/(σ₂ + σ₃)

x_da, y_da, z_da = assimilate(ds, Xic, xdata, ydata, zdata, K)

figure()
plot(t, Traj_true[:, 1], label="True")
plot(t, x_da, label="Assimilated")
plot(tdata, xdata, "ro")
legend()
grid(true)

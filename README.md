### Data assimilation demo in Julia

Based on Nathan Kutz lecture. https://www.youtube.com/watch?v=izsAf9dD_WE&t=15s

To activate the project, in Julia package manager (press "]" to enter pkg manager).

```bash
$ git clone https://gitlab.com/Balinus/assimilation-demo.git
$ cd assimilation-demo
$ julia
```

```julia
pkg> activate .
```

Then explore.
